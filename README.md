# kart-service-api

## Rodando a aplicação
- Baixar o software da MuleSoft (AnypointStudio versão 6.5.2) -> https://www.mulesoft.com/lp/dl/studio/previous   (Baixar mule 3.9 e Studio 6.5) OBS: O aplicativo utiliza o **RunTime na versão 3.9.1 EE**
- Necessário ter **JAVA 8** e **Maven** instalado na máquina
- Utilizar uma máquina **linux**
- Baixar o projeto do Bitbucket -> https://bitbucket.org/Henriquegmota/kart-service-api/src/develop/
- descompactar o arquivo **repository.zip** na pasta **.m2** que fica localizado na pasta do usuario. 
- Após baixar o Software, extrair ele em qualquer lugar, acessar o arquivo e executar  **'AnypointStudio'**
- Quando abrir o AnypointStudio clicar em **File -> Import -> Anypoint Studio -> Maven-based MuleProject from pom.xml**
- Onde tem o **POM File**, pesquisar o arquivo que está **dentro do projeto** baixado e selecionar o **pom.xml**
- Navegue até a pasta **src/main/app** e abrir qualquer arquivo **.xml**
- Após abrir o arquivo dentro dele clicar com **botão direito** do mouse, e selecionar **Run project kart-service-api**
- Após alguns instantes, olhando o console irá aparecer :
-      * kart-service-api                     * default                        * DEPLOYED          
- A partir daqui a aplicação já estará rodando no **localhost:8081** , porém existe **3** tipos diferêntes de testar o serviço.
## 1º Tipo usando arquivo .txt
- Jogar o arquivo de log **.txt** no diretório  **src/main/resources**
- Após jogar o aquivo, ele será processado automaticamente e será movido para pasta **src/main/resources/log**
- A resposta da aplicação irá se encontra na pasta **src/main/resources/output** com o mesmo nome do arquivo porém com a extensão **.json**

## 2º  Tipo utilizando uma chamada HTTP como POST contendo um json como corpo
- Logo quando abrir o projeto na pasta raiz irá conter um arquivo chamada "**GymPass - kart.postman_collection.json**" este arquivo foi exportado do PostMan, onde nele conterá os dois métodos POST desta aplicação.
- Abrir o PostMan clicar em import e arrastar o arquivo mencionado acima
- abrir a requisição chamada '**Post-json-kart**'
- clicar em Body, onde se encontra o json de request, após isto clicar em Send, e como retorno obterá os dados da corrida.

## 3º Tipo utilizando HTTP como POST contendo um form-data como requisição
- Utilizando o mesmo PostMan do tipo anterior, abrir a requisição '**Post-form-data-kart**'
- Cliclar em Body para selecionar o mesmo arquivo **.txt** enviado no 1º Tipo
- Clicar em Send
- O retorno obtido dessa requisição estará na pasta **src/main/resources/form-data-output**
- Nesta pasta terá um arquivo com o mesmo nome do arquivo enviado, porém com a extensão **.json**


# Testes

Existe **2** métodos para rodar os testes da aplicação

## 1º método
- Abrir o arquivo **kart-test-suite.xml** que se encontra na pasta **src/test/munit**
- Após abrir, clicar com botão direito e clicar em **Run MUnit suite**
-  No canto inferior esquerdo irá aparecer os resultados dos testes

## 2º método
- Abrir o terminar na pastar raiz do projeto
-  Executar o comando **mvn clean install**
- Após a finalização do comando, no terminal irá aparecer **BUILD SUCCESS** e logo em cima está as informações dos testes


# Demonstração
- dentro do projeto na pasta raiz, existe um arquivo chamado **tutorial-demonstracao.flv** este arquivo é um vídeo demonstrando a importação do projeto dentro do AnypointStudio e a execução dos 3 tipos de utilização da API

# Autor
##Henrique de Godoy Mota
- Email: henriquegodoymota@gmail.com
- Linkedin: https://www.linkedin.com/in/henrique-godoy-mota-667896a8/
