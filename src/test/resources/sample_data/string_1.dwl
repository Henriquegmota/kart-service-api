%dw 1.0
%output application/java
---
[
  {
    "hora": "23:49:08.277",
    "codigoPiloto": "038",
    "piloto": "F.MASSA",
    "numeroVolta": "1",
    "tempoVolta": "1:02.852",
    "velocidadeMediaVolta": "44,275"
  },
  {
    "hora": "23:49:10.858",
    "codigoPiloto": "033",
    "piloto": "R.BARRICHELLO",
    "numeroVolta": "1",
    "tempoVolta": "1:04.352",
    "velocidadeMediaVolta": "43,243"
  },
  {
    "hora": "23:49:11.075",
    "codigoPiloto": "002",
    "piloto": "K.RAIKKONEN",
    "numeroVolta": "1",
    "tempoVolta": "1:04.108",
    "velocidadeMediaVolta": "43,408"
  },
  {
    "hora": "23:49:12.667",
    "codigoPiloto": "023",
    "piloto": "M.WEBBER",
    "numeroVolta": "1",
    "tempoVolta": "1:04.414",
    "velocidadeMediaVolta": "43,202"
  },
  {
    "hora": "23:49:30.976",
    "codigoPiloto": "015",
    "piloto": "F.ALONSO",
    "numeroVolta": "1",
    "tempoVolta": "1:18.456",
    "velocidadeMediaVolta": "35,47"
  },
  {
    "hora": "23:50:11.447",
    "codigoPiloto": "038",
    "piloto": "F.MASSA",
    "numeroVolta": "2",
    "tempoVolta": "1:03.170",
    "velocidadeMediaVolta": "44,053"
  },
  {
    "hora": "23:50:14.860",
    "codigoPiloto": "033",
    "piloto": "R.BARRICHELLO",
    "numeroVolta": "2",
    "tempoVolta": "1:04.002",
    "velocidadeMediaVolta": "43,48"
  },
  {
    "hora": "23:50:15.057",
    "codigoPiloto": "002",
    "piloto": "K.RAIKKONEN",
    "numeroVolta": "2",
    "tempoVolta": "1:03.982",
    "velocidadeMediaVolta": "43,493"
  },
  {
    "hora": "23:50:17.472",
    "codigoPiloto": "023",
    "piloto": "M.WEBBER",
    "numeroVolta": "2",
    "tempoVolta": "1:04.805",
    "velocidadeMediaVolta": "42,941"
  },
  {
    "hora": "23:50:37.987",
    "codigoPiloto": "015",
    "piloto": "F.ALONSO",
    "numeroVolta": "2",
    "tempoVolta": "1:07.011",
    "velocidadeMediaVolta": "41,528"
  },
  {
    "hora": "23:51:14.216",
    "codigoPiloto": "038",
    "piloto": "F.MASSA",
    "numeroVolta": "3",
    "tempoVolta": "1:02.769",
    "velocidadeMediaVolta": "44,334"
  },
  {
    "hora": "23:51:18.576",
    "codigoPiloto": "033",
    "piloto": "R.BARRICHELLO",
    "numeroVolta": "3",
    "tempoVolta": "1:03.716",
    "velocidadeMediaVolta": "43,675"
  },
  {
    "hora": "23:51:19.044",
    "codigoPiloto": "002",
    "piloto": "K.RAIKKONEN",
    "numeroVolta": "3",
    "tempoVolta": "1:03.987",
    "velocidadeMediaVolta": "43,49"
  },
  {
    "hora": "23:51:21.759",
    "codigoPiloto": "023",
    "piloto": "M.WEBBER",
    "numeroVolta": "3",
    "tempoVolta": "1:04.287",
    "velocidadeMediaVolta": "43,287"
  },
  {
    "hora": "23:51:46.691",
    "codigoPiloto": "015",
    "piloto": "F.ALONSO",
    "numeroVolta": "3",
    "tempoVolta": "1:08.704",
    "velocidadeMediaVolta": "40,504"
  },
  {
    "hora": "23:52:01.796",
    "codigoPiloto": "011",
    "piloto": "S.VETTEL",
    "numeroVolta": "1",
    "tempoVolta": "3:31.315",
    "velocidadeMediaVolta": "13,169"
  },
  {
    "hora": "23:52:17.003",
    "codigoPiloto": "038",
    "piloto": "F.MASS",
    "numeroVolta": "4",
    "tempoVolta": "1:02.787",
    "velocidadeMediaVolta": "44,321"
  },
  {
    "hora": "23:52:22.586",
    "codigoPiloto": "033",
    "piloto": "R.BARRICHELLO",
    "numeroVolta": "4",
    "tempoVolta": "1:04.010",
    "velocidadeMediaVolta": "43,474"
  },
  {
    "hora": "23:52:22.120",
    "codigoPiloto": "002",
    "piloto": "K.RAIKKONEN",
    "numeroVolta": "4",
    "tempoVolta": "1:03.076",
    "velocidadeMediaVolta": "44,118"
  },
  {
    "hora": "23:52:25.975",
    "codigoPiloto": "023",
    "piloto": "M.WEBBER",
    "numeroVolta": "4",
    "tempoVolta": "1:04.216",
    "velocidadeMediaVolta": "43,335"
  },
  {
    "hora": "23:53:06.741",
    "codigoPiloto": "015",
    "piloto": "F.ALONSO",
    "numeroVolta": "4",
    "tempoVolta": "1:20.050",
    "velocidadeMediaVolta": "34,763"
  },
  {
    "hora": "23:53:39.660",
    "codigoPiloto": "011",
    "piloto": "S.VETTEL",
    "numeroVolta": "2",
    "tempoVolta": "1:37.864",
    "velocidadeMediaVolta": "28,435"
  },
  {
    "hora": "23:54:57.757",
    "codigoPiloto": "011",
    "piloto": "S.VETTEL",
    "numeroVolta": "3",
    "tempoVolta": "1:18.097",
    "velocidadeMediaVolta": "35,633"
  }
]